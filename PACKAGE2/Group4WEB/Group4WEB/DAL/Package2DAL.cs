﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using Group4WEB.Models;

namespace Group4WEB.DAL
{
    public class Package2DAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        public Package2DAL()
        {
            //Read ConnectionString from appsettings.json file     
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AirFlightsConnectionString");
            //Instantiate a SqlConnection object with the Connection String read.        
            conn = new SqlConnection(strConn);
        }

        public List<FlightRoute> GetFlightRoutes()
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM FlightRoute ORDER BY RouteID";
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<FlightRoute> routelist = new List<FlightRoute>();

            while (reader.Read())
            {
                routelist.Add(
                    new FlightRoute
                    {
                        RouteID = reader.GetInt32(0),    //0: 1st column      
                        DepartureCity = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        DepartureCountry = reader.GetString(2), //2: 3rd column           
                        ArrivalCity = reader.GetString(3),     //3: 4th column                     
                        ArrivalCountry = reader.GetString(4),   //5: 6th column                   
                        FlightDuration = reader.GetInt32(5),  //6: 7th column                  
                        
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return routelist;
        }

        public List<Aircraft> GetAircraft()
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM Aircraft ORDER BY AircraftID";
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<Aircraft> aircraftlist = new List<Aircraft>();

            while (reader.Read())
            {
                aircraftlist.Add(
                    new Aircraft
                    {
                        AircraftID = reader.GetInt32(0),    //0: 1st column      
                        MakeModel = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        NumEconomySeat = reader.GetInt32(2), //2: 3rd column           
                        NumBusinessSeat = reader.GetInt32(3),     //3: 4th column                     
                        DateLastMaintenance = reader.GetDateTime(4),   //5: 6th column                   
                        Status = reader.GetString(5),  //6: 7th column                  
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return aircraftlist;
        }

        public List<FlightSchedule> GetFlightSchedules()
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM FlightSchedule ORDER BY ScheduleID";
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<FlightSchedule> scheduleList = new List<FlightSchedule>();

            while (reader.Read())
            {
                scheduleList.Add(
                    new FlightSchedule
                    {
                        ScheduleID = reader.GetInt32(0),    //0: 1st column      
                        FlightNumber = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        RouteID = reader.GetInt32(2), //2: 3rd column           
                        AircraftID = reader.GetInt32(3),     //3: 4th column                     
                        DepartureDateTime = reader.GetDateTime(4),   //5: 6th column                   
                        ArrivalDateTime = reader.GetDateTime(5),  //6: 7th column  
                        EconomyClassPrice = Convert.ToDouble(reader.GetDecimal(6)),  //6: 7th column   
                        BusinessClassPrice = Convert.ToDouble(reader.GetDecimal(7)),  //6: 7th column   
                        Status = reader.GetString(8),  //6: 7th column   
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return scheduleList;
        }

        public int AddFlightSchedule(FlightSchedule flightSchedule)
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            System.Diagnostics.Debug.WriteLine(flightSchedule.RouteID);
            System.Diagnostics.Debug.WriteLine(flightSchedule.RouteID);
            //Specify an INSERT SQL statement which will   
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO FlightSchedule (FlightNumber, RouteID, AircraftID,
                                DepartureDateTime, ArrivalDateTime, EconomyClassPrice, BusinessClassPrice, Status)
                                OUTPUT INSERTED.ScheduleID 
                                VALUES(@flightNumber, @routeID, @aircraftID,
                                @departureDateTime, @arrivalDateTime, @economyClassPrice, @businessClassPrice, @status)";
            //Define the parameters used in SQL statement, value for each parameter  
            //is retrieved from respective class's property.
           
            cmd.Parameters.AddWithValue("@flightNumber", flightSchedule.FlightNumber);
            cmd.Parameters.AddWithValue("@routeID", flightSchedule.RouteID);
            cmd.Parameters.AddWithValue("@aircraftID", flightSchedule.AircraftID);
            cmd.Parameters.AddWithValue("@departureDateTime", flightSchedule.DepartureDateTime);
            cmd.Parameters.AddWithValue("@arrivalDateTime", flightSchedule.ArrivalDateTime);
            cmd.Parameters.AddWithValue("@economyClassPrice", flightSchedule.EconomyClassPrice);
            cmd.Parameters.AddWithValue("@businessClassPrice", flightSchedule.BusinessClassPrice);
            cmd.Parameters.AddWithValue("@status", flightSchedule.Status);

            //A connection to database must be opened before any operations made.   
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated   
            //StaffID after executing the INSERT SQL statement   
            flightSchedule.ScheduleID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations. 
            conn.Close();

            //Return id when no error occurs.   
            return flightSchedule.ScheduleID;
        }

        public int AddFlightRoute(FlightRoute flightroute)
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will   
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO FlightRoute (DepartureCity, DepartureCountry, ArrivalCity,
                                ArrivalCountry, FlightDuration)
                                OUTPUT INSERTED.RouteID 
                                VALUES(@departureCity, @departureCountry, @arrivalCity,
                                @arrivalCountry, @flightDuration)";
            //Define the parameters used in SQL statement, value for each parameter  
            //is retrieved from respective class's property.

            cmd.Parameters.AddWithValue("@departureCity", flightroute.DepartureCity);
            cmd.Parameters.AddWithValue("@departureCountry", flightroute.DepartureCountry);
            cmd.Parameters.AddWithValue("@arrivalCity", flightroute.ArrivalCity);
            cmd.Parameters.AddWithValue("@arrivalCountry", flightroute.ArrivalCountry);
            cmd.Parameters.AddWithValue("@flightDuration", flightroute.FlightDuration);

            //A connection to database must be opened before any operations made.   
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated   
            //StaffID after executing the INSERT SQL statement   
            flightroute.RouteID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations. 
            conn.Close();

            //Return id when no error occurs.   
            return flightroute.RouteID;
        }

        public void DeleteFlightSchedule(int scheduleid)
        {

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM FlightSchedule WHERE ScheduleID = @selectScheduleID";
            cmd.Parameters.AddWithValue("@selectScheduleID", scheduleid);
            System.Diagnostics.Debug.WriteLine(scheduleid);
            //Open a database connection   
            conn.Open();         
            //Execute the DELETE SQL to remove the staff record    
            cmd.ExecuteNonQuery();

            //Close database connection  
            conn.Close();

            //Return number of row of staff record updated or deleted  
            return;
        }

        public FlightSchedule FindSpecificFlightSchedule(int scheduleid)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement    
            //to delete a staff record specified by a Staff ID 
            FlightSchedule current = new FlightSchedule();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE ScheduleID = @selectScheduleID";
            cmd.Parameters.AddWithValue("@selectScheduleID", scheduleid);

            
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    current.ScheduleID = scheduleid;
                    current.FlightNumber = !reader.IsDBNull(1) ? reader.GetString(1) : "NO VALUE";
                    // (char) 0 - ASCII Code 0 - null value       
                    current.RouteID = !reader.IsDBNull(2) ? reader.GetInt32(2) : 0;
                    current.AircraftID = !reader.IsDBNull(3) ? reader.GetInt32(3) : 1020202;
                    current.DepartureDateTime = !reader.IsDBNull(4) ? reader.GetDateTime(4) : (DateTime?) null;
                    current.ArrivalDateTime = !reader.IsDBNull(5) ? reader.GetDateTime(5) : (DateTime?)null;
                    current.EconomyClassPrice = !reader.IsDBNull(6) ? Convert.ToDouble(reader.GetDecimal(6)) : 0;
                    current.BusinessClassPrice = !reader.IsDBNull(7) ? Convert.ToDouble(reader.GetDecimal(7)) : 0;
                    current.Status = !reader.IsDBNull(8) ? reader.GetString(8) : "NO VALUE";
                }
            }

            

            //Close database connection  
            conn.Close();

            //Return number of row of staff record updated or deleted  
            return current;
        }

        public FlightRoute FindSpecificFlightRoute(int routeid)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement    
            //to delete a staff record specified by a Staff ID 
            FlightRoute current = new FlightRoute();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM FlightRoute WHERE RouteID = @selectRouteID";
            cmd.Parameters.AddWithValue("@selectRouteID", routeid);


            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    current.RouteID = routeid;
                    current.DepartureCity = !reader.IsDBNull(1) ? reader.GetString(1) : "NO VALUE";
                    // (char) 0 - ASCII Code 0 - null value       
                    current.DepartureCountry = !reader.IsDBNull(2) ? reader.GetString(2) : "NO VALUE";
                    current.ArrivalCity = !reader.IsDBNull(3) ? reader.GetString(3) : "NO VALUE";
                    current.ArrivalCountry = !reader.IsDBNull(4) ? reader.GetString(4) : "NO VALUE";
                    current.FlightDuration = !reader.IsDBNull(5) ? reader.GetInt32(5) : 0;
                }
            }



            //Close database connection  
            conn.Close();

            //Return number of row of staff record updated or deleted  
            return current;
        }

        public List<Booking> GetBookingsForSchedule(int id)
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM Booking WHERE ScheduleID = @scheduleID";
            cmd.Parameters.AddWithValue("@scheduleID", id);
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<Booking> bookingList = new List<Booking>();

            while (reader.Read())
            {
                bookingList.Add(
                    new Booking
                    {
                        BookingID = reader.GetInt32(0),    //0: 1st column      
                        CustomerID = reader.GetInt32(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        ScheduleID = reader.GetInt32(2), //2: 3rd column           
                        PassengerName = reader.GetString(3),     //3: 4th column          
                        PassportNumber = reader.GetString(4),
                        Nationality = reader.GetString(5),   //5: 6th column                   
                        SeatClass = reader.GetString(6),  //6: 7th column  
                        AmtPayable = Convert.ToDouble(reader.GetDecimal(7)),  //6: 7th column   
                        Remarks = !reader.IsDBNull(8) ? reader.GetString(8) : "NO REMARK",  //6: 7th column   
                        DateTimeCreated = reader.GetDateTime(9),  //6: 7th column   
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return bookingList;
        }

        public List<FlightSchedule> GetFlightSchedulesForRoutes(int id)
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE RouteID = @givenrouteID ORDER BY ScheduleID";
            cmd.Parameters.AddWithValue("@givenrouteID", id);
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<FlightSchedule> scheduleList = new List<FlightSchedule>();

            while (reader.Read())
            {
                scheduleList.Add(
                    new FlightSchedule
                    {
                        ScheduleID = reader.GetInt32(0),    //0: 1st column      
                        FlightNumber = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        RouteID = reader.GetInt32(2), //2: 3rd column           
                        AircraftID = reader.GetInt32(3),     //3: 4th column                     
                        DepartureDateTime = reader.GetDateTime(4),   //5: 6th column                   
                        ArrivalDateTime = reader.GetDateTime(5),  //6: 7th column  
                        EconomyClassPrice = Convert.ToDouble(reader.GetDecimal(6)),  //6: 7th column   
                        BusinessClassPrice = Convert.ToDouble(reader.GetDecimal(7)),  //6: 7th column   
                        Status = reader.GetString(8),  //6: 7th column   
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return scheduleList;
        }

        public int UpdateStatus(FlightSchedule flightschedule)
        {
            //Create a SqlCommand object from connection object     
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement    
            cmd.CommandText = @"UPDATE FlightSchedule SET Status=@status WHERE ScheduleID = @selectedScheduleID";
            //Define the parameters used in SQL statement, value for each parameter   
            //is retrieved from respective class's property.    
            cmd.Parameters.AddWithValue("@status", flightschedule.Status);        
            cmd.Parameters.AddWithValue("@selectedScheduleID", flightschedule.ScheduleID);
            //Open a database connection     
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE  
            int count = cmd.ExecuteNonQuery();
            //Close the database connection    
            conn.Close();

            return count;
        }

    }
}
