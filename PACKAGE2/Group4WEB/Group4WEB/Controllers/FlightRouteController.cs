﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Group4WEB.Models;
using Group4WEB.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Group4WEB.Controllers
{
    public class FlightRouteController : Controller
    {
        private Package2DAL package2DB = new Package2DAL();

        // GET: FlightRoute
        public ActionResult Index()
        {
            List<FlightRoute> routeList = package2DB.GetFlightRoutes();
            return View(routeList);
        }

        // GET: FlightRoute/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: FlightRoute/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FlightRoute/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FlightRoute flightroute)
        {
            if (ModelState.IsValid)
            {
                //Add FlightRoute record to database
                flightroute.RouteID = package2DB.AddFlightRoute(flightroute);
                //Redirect user to FlightSchedule/Index view 
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view         
                //to display error message 
                return View(flightroute);
            }
        }

        // GET: FlightRoute/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: FlightRoute/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FlightRoute/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: FlightRoute/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}