﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Group4WEB.Models
{
    public class FlightSchedule
    {
        [Required]
        public int ScheduleID { get; set; }
        [Required]
        public string FlightNumber { get; set; }
        [Required]
        public int RouteID { get; set; }
        [Required]
        public int AircraftID { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime? DepartureDateTime { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? ArrivalDateTime { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public double EconomyClassPrice { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public double BusinessClassPrice { get; set; }
        [Required]
        public string Status { get; set; }
    }
}
