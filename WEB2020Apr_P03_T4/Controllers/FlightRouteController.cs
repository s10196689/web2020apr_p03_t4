﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WEB2020Apr_P03_T4.Models;
using WEB2020Apr_P03_T4.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WEB2020Apr_P03_T4.Controllers
{
    public class FlightRouteController : Controller
    {
        private Package2DAL package2DB = new Package2DAL();

        // GET: FlightRoute
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }

            List<FlightRoute> routeList = package2DB.GetFlightRoutes();
            return View(routeList);
        }

        // GET: FlightRoute/Details/5
        public ActionResult Details(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // GET: FlightRoute/Create
        public ActionResult Create()
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // POST: FlightRoute/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FlightRoute flightroute)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                //qh - Add FlightRoute record to database
                flightroute.RouteID = package2DB.AddFlightRoute(flightroute);
                //Redirect user to FlightSchedule/Index view 
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view         
                //to display error message 
                return View(flightroute);
            }
        }

        // GET: FlightRoute/Edit/5
        //qh - UNUSED
        public ActionResult Edit(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // POST: FlightRoute/Edit/5
        //qh - UNUSED
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FlightRoute/Delete/5 UNUSED
        //qh - UNUSED
        public ActionResult Delete(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // POST: FlightRoute/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}