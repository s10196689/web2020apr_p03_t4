﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WEB2020Apr_P03_T4.Models;
using WEB2020Apr_P03_T4.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WEB2020Apr_P03_T4.Controllers
{
    public class FlightScheduleController : Controller
    {
        private Package2DAL package2DB = new Package2DAL();

        // GET: FlightSchedule
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            //qh - gets all flight schedules
            List<FlightSchedule> scheduleList = package2DB.GetFlightSchedules();
            return View(scheduleList);
        }

        // GET: FlightSchedule/Details/5
        public ActionResult Details(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // GET: FlightSchedule/Create
        public ActionResult Create(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            FlightSchedule created = new FlightSchedule();
            created.RouteID = id;

            System.Diagnostics.Debug.WriteLine(id);
            System.Diagnostics.Debug.WriteLine(created.RouteID);
            //qh - data to be showed in the html page
            ViewData["RouteList"] = getroutes();
            ViewData["AircraftList"] = getaircraft();
            ViewData["ID"] = id;
            return View(created);
        }

        // POST: FlightSchedule/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FlightSchedule flightschedule)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            System.Diagnostics.Debug.WriteLine(flightschedule.RouteID);
            ViewData["RouteList"] = getroutes();
            ViewData["AircraftList"] = getaircraft();

            //qh- make sure departure date cannot be today or before today
            if (flightschedule.DepartureDateTime.Value.Date <= DateTime.Today.Date && flightschedule.DepartureDateTime.Value.Year <= DateTime.Today.Year)
            {
                ViewData["ID"] = flightschedule.RouteID;
                ViewData["Error"] = "Date must be one day after today!";
                return View(flightschedule);
            }

            //qh - auto adds the duration to arrival date time
            FlightRoute current = package2DB.FindSpecificFlightRoute(flightschedule.RouteID);
            int duration = current.FlightDuration;
            flightschedule.ArrivalDateTime = flightschedule.DepartureDateTime.Value.AddHours(duration);

            if (ModelState.IsValid)
            {
                //Add FlightSchedule record to database
                flightschedule.ScheduleID = package2DB.AddFlightSchedule(flightschedule);
                //Redirect user to FlightSchedule/Index view 
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view         
                //to display error message 
                return View(flightschedule);
            }
        }


        // GET: FlightSchedule/Edit/5
        public ActionResult EditStatus(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            FlightSchedule current = package2DB.FindSpecificFlightSchedule(id);
            System.Diagnostics.Debug.WriteLine(current.ScheduleID);
            return View(current);
        }

        // POST: FlightSchedule/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditStatus(FlightSchedule current)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            //qh - db code to update status
            System.Diagnostics.Debug.WriteLine(current.ScheduleID);
            package2DB.UpdateStatus(current);
            return RedirectToAction("Index");
        }

        // GET: FlightSchedule/Delete/5
        public ActionResult Delete(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            FlightSchedule current = package2DB.FindSpecificFlightSchedule(id);
            System.Diagnostics.Debug.WriteLine(current.ScheduleID);

            return View(current);
        }

        // POST: FlightSchedule/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(FlightSchedule current)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            System.Diagnostics.Debug.WriteLine(current.ScheduleID);
            package2DB.DeleteFlightSchedule(current.ScheduleID);
            return RedirectToAction("Index");
        }

        //for view data
        public List<SelectListItem> getroutes()
        {
            List<FlightRoute> routelist = package2DB.GetFlightRoutes();
            List<SelectListItem> routeselectlist = new List<SelectListItem>();
            foreach (FlightRoute i in routelist)
            {
                routeselectlist.Add(new SelectListItem { Value = Convert.ToString(i.RouteID), Text = Convert.ToString(i.RouteID) });
            }
            return routeselectlist;
        }
        //for view data
        public List<SelectListItem> getaircraft()
        {
            List<Aircraft> aircrafts = package2DB.GetAircraft();
            List<SelectListItem> aircraftselectlist = new List<SelectListItem>();
            foreach (Aircraft i in aircrafts)
            {
                aircraftselectlist.Add(new SelectListItem { Value = Convert.ToString(i.AircraftID), Text = Convert.ToString(i.AircraftID) });
            }
            return aircraftselectlist;
        }

        public ActionResult ListSpecific(int id)
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            List<FlightSchedule> scheduleList = package2DB.GetFlightSchedulesForRoutes(id);
            return View(scheduleList);
        }
    }
}