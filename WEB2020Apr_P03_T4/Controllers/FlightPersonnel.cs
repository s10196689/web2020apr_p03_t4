﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using WEB2020Apr_P03_T4.Dal;
using WEB2020Apr_P03_T4.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WEB2020Apr_P03_T4.Controllers
{
    public class FlightPersonnel : Controller
    {
        Package3DAL Package3DAL = new Package3DAL();
        public IActionResult Index()
        {
            List<Staff> staffList = Package3DAL.GetStaff();
            List<StaffView> staffviewList = new List<StaffView>();

            foreach(Staff staff in staffList)
            {
                StaffView staffview  = MapToStaffViewModel(staff);
                staffviewList.Add(staffview);
            }

            return View(staffviewList);
        }

        public ActionResult Create()
        {
            ViewData["vocationList"] = GetVocation();
            ViewData["genderList"] = GetGender();
            ViewData["statusList"] = GetStatus();
            return View();
        }

        private List<SelectListItem> GetVocation()
        {
            List<SelectListItem> vocationList = new List<SelectListItem>();
            vocationList.Add(new SelectListItem { Value = "Pilot", Text = "Pilot" });
            vocationList.Add(new SelectListItem { Value = "Flight Attendant", Text = "Flight Attendant" });

            return vocationList;
        }

        private List<SelectListItem> GetGender()
        {
            List<SelectListItem> genderList = new List<SelectListItem>();
            genderList.Add(new SelectListItem { Value = "M", Text = "M" });
            genderList.Add(new SelectListItem { Value = "F", Text = "F" });

            return genderList;
        }

        private List<SelectListItem> GetStatus()
        {
            List<SelectListItem> statusList = new List<SelectListItem>();
            statusList.Add(new SelectListItem { Value = "Active", Text = "Active" });
            statusList.Add(new SelectListItem { Value = "Inactive", Text = "Inactive" });

            return statusList;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Staff staff)
        {
            if (ModelState.IsValid)
            {
                staff.StaffID = Package3DAL.AddStaff(staff);
                return RedirectToAction("Index");
            }

            else
            {
                return View();
            }
        }
        public StaffView MapToStaffViewModel(Staff staff)
        {
            List<FlightCrew> flightList = Package3DAL.GetFlight();
            int ScheduleID = 0;

            foreach(FlightCrew flight in flightList)
            {
                if(flight.StaffID == staff.StaffID)
                {
                    ScheduleID = flight.ScheduleID;
                }

            }

            StaffView staffView = new StaffView
            {
                StaffName = staff.StaffName,
                Vocation = staff.Vocation,
                ScheduleID = ScheduleID,
                Status = staff.Status
            };

            return staffView;
        }
    }
}
