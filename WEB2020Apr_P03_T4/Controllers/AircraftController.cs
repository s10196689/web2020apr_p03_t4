﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Group4WEB.DAL;
using WEB2020Apr_P03_T4.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Security.Cryptography.Xml;

namespace Group4WEB.Controllers
{
    public class AircraftController : Controller
    {
        Package4DAL packageDAL = new Package4DAL();
        // GET: Aircraft
        public ActionResult Index()
        {
            List<AircraftViewModel> aircraftViewModels = new List<AircraftViewModel>();
            List<Aircraft> aircrafts = packageDAL.GetAircraft();
            foreach (Aircraft a in aircrafts)
            {
                AircraftViewModel aircraftVM = MapToAircraftVM(a);
                aircraftViewModels.Add(aircraftVM);

            }
            return View(aircraftViewModels);

        }
        public AircraftViewModel MapToAircraftVM(Aircraft aircraft)
        {
            List<FlightScheduleViewModel> AllFlightSchedules = new List<FlightScheduleViewModel>();
            List<FlightScheduleViewModel> AircraftFlights = new List<FlightScheduleViewModel>();
            string departureCity = "";
            string arrivalCity = "";
            DateTime? departuretime = null, arrivalTime = null;
            List<FlightSchedule> flights = packageDAL.GetFlightSchedules();
            List<FlightRoute> routes = packageDAL.GetFlightRoutes();
            foreach (FlightSchedule f in flights)
            {

                if (f.AircraftID == aircraft.AircraftID)
                {

                    departuretime = f.DepartureDateTime;
                    arrivalTime = f.ArrivalDateTime;
                    foreach (FlightRoute r in routes)
                    {
                        if (r.RouteID == f.RouteID)
                        {
                            FlightScheduleViewModel vm = new FlightScheduleViewModel
                            {
                                ArrivalCity = r.ArrivalCity,
                                DepartureCity = r.DepartureCity,
                                ArrivalDateTime = f.ArrivalDateTime,
                                DepartureDateTime = f.DepartureDateTime,
                                FlightNumber = f.FlightNumber,
                                ScheduleID = f.ScheduleID
                            };
                            AircraftFlights.Add(vm);
                            departureCity = r.DepartureCity;
                            arrivalCity = r.ArrivalCity;
                            break;
                        }
                    }

                }

            }
            foreach (FlightSchedule f in flights)
            {

                foreach (FlightRoute r in routes)
                {
                    if (f.RouteID == r.RouteID && f.AircraftID != aircraft.AircraftID)
                    {
                        FlightScheduleViewModel vm = new FlightScheduleViewModel
                        {
                            ArrivalCity = r.ArrivalCity,
                            DepartureCity = r.DepartureCity,
                            ArrivalDateTime = f.ArrivalDateTime,
                            DepartureDateTime = f.DepartureDateTime,
                            FlightNumber = f.FlightNumber,
                            AircraftID = f.AircraftID,
                            ScheduleID = f.ScheduleID
                        };
                        AllFlightSchedules.Add(vm);
                    }

                }

            }

            AircraftViewModel aircraftVM = new AircraftViewModel
            {
                AircraftID = aircraft.AircraftID,
                DateOfLastMaintenance = aircraft.DateLastMaintenance,
                MakeModel = aircraft.MakeModel,
                NumBusinessSeat = aircraft.NumBusinessSeat,
                NumEconomySeat = aircraft.NumEconomySeat,
                DateLastMaintenance = aircraft.DateLastMaintenance,
                DepartureCity = departureCity,
                ArrivalCity = arrivalCity,
                DepartureDateTime = departuretime,
                ArrivalDateTime = arrivalTime,
                AircraftHistory = AircraftFlights,
                AllFlights = AllFlightSchedules,
                Status = aircraft.Status



            };
            return aircraftVM;


        }


        // GET: Aircraft/Details/5
        public ActionResult Details(int id)
        {
            Aircraft aircraft = packageDAL.GetAircraftDetails(id);
            AircraftViewModel aircraftVM = MapToAircraftVM(aircraft);
            return View(aircraftVM);
        }

        // GET: Aircraft/Create
        public ActionResult Create()
        {
            ViewData["StatusList"] = GetStatus();
            return View();


        }

        private List<SelectListItem> GetStatus()
        {
            List<SelectListItem> statuses = new List<SelectListItem>();
            statuses.Add(new SelectListItem { Value = "Operational", Text = "Operational" });
            statuses.Add(new SelectListItem { Value = "Under Maintenance", Text = "Under Maintenance" });
            return statuses;

        }
        private List<SelectListItem> GetAircraft()
        {
            List<SelectListItem> statuses = new List<SelectListItem>();
            List<Aircraft> aircrafList = packageDAL.GetAircraft();
            foreach (Aircraft a in aircrafList)
            {
                if (a.Status == "Operational")
                {
                    statuses.Add(new SelectListItem { Value = a.AircraftID.ToString(), Text = a.AircraftID.ToString() });
                }
            }
            return statuses;
        }
        // POST: Aircraft/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Aircraft aircraft)
        {
            if (ModelState.IsValid)
            {
                aircraft.AircraftID = packageDAL.AddAircraft(aircraft);

                return RedirectToAction("Index");
            }
            else
            {
                return View(aircraft);
            }
        }
        public ActionResult Edit (int id)
        {
            ViewData["AircraftID"] = GetAircraft();
            FlightSchedule f = packageDAL.FindSpecificFlightSchedule(id);
            return View(f);
        }
        // POST: Aircraft/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int AircraftID, int ScheduleID)
        {
            packageDAL.AssignAircraft(AircraftID,ScheduleID);
            return RedirectToAction("Index");

        }
        public ActionResult EditStatus(int id)
        {
            ViewData["StatusList"] = GetStatus();
            Aircraft aircraft = packageDAL.GetAircraftDetails(id);
            return View(aircraft);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditStatus(Aircraft aircraft)
        {
            if (ModelState.IsValid)
            {
                List<FlightSchedule> fList = packageDAL.GetAircraftFlights(aircraft.AircraftID);
                foreach (FlightSchedule f in fList)
                {
                    if (f.DepartureDateTime>DateTime.Now)
                    {
                        TempData["Message"] = "Aircraft has a pending flight!";
                        return View();
                    }
                    else
                    {
                        packageDAL.UpdateStatus(aircraft);
                    }
                }
                

                return RedirectToAction("Index");
            }
            else
            {
                return View(aircraft);
            }
        }
    }
}