﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WEB2020Apr_P03_T4.Models;
using Microsoft.AspNetCore.Http;
using WEB2020Apr_P03_T4.DAL;

namespace WEB2020Apr_P03_T4.Controllers
{
    public class CustomerController : Controller
    {
        private PackageOneDAL customerContext = new PackageOneDAL();
        private Package2DAL bookingContext = new Package2DAL();
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult RegisterView()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterView(Customer customer)
        {
            if (customerContext.IsEmailExist(customer.EmailAddr))
            {
                customerContext.Add(customer);
                return RedirectToAction("Login","Home");
            }

            else
            {
                TempData["Message"] = "Email already exists";
                return RedirectToAction("RegisterView");
            }
        }
        public ActionResult Changepw(int id)
        {
            Customer c1 = new Customer();
            c1 = customerContext.FindCustById(id);
            return View(c1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Changepw(Customer customer)
        {
            customerContext.Updatepw(customer);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult FlightBooking()
        {
            List<FlightSchedule> flightSchedules = new List<FlightSchedule>();
            flightSchedules = bookingContext.GetFlightSchedules();
            return View(flightSchedules);
        }

        public ActionResult BookingForm(int id)
        {
            Booking booking = new Booking();
            booking.ScheduleID = id;
            return View(booking);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BookingForm(Booking booking)
        {
            //Get country list for drop-down list
            //in case of the need to return to Create.cshtml view
            //ViewData["bookingList"] = BookingForm();
            if (ModelState.IsValid)
            {
                //string CustID = HttpContext.Session.GetString("LoginID");
                customerContext.AddBooking(booking);
                //Redirect user to Index view
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(booking);
            }

        }
    }
}