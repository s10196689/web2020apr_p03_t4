﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WEB2020Apr_P03_T4.Models;

namespace WEB2020Apr_P03_T4.Controllers
{
    //qh - done by me to for the team API component
    public class CountryController : Controller
    {
        public async Task<ActionResult> Index()
        {
            //qh - api for coronavirus
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.covid19api.com");
            HttpResponseMessage response = await client.GetAsync("/countries");
            if (response.IsSuccessStatusCode)
            { 
                string data = await response.Content.ReadAsStringAsync();
                List<Country> countryList = JsonConvert.DeserializeObject<List<Country>>(data);
                return View(countryList);
            }
            else
            { 
                return View(new List<Country>());
            }
        }

        public async Task<ActionResult> Details(string slug, string country)
        {
            //qh - slug is the country name but simplified because the api link uses slug instead
            //qh - api for coronavirus
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.covid19api.com");
            //qh - get days for data
            DateTime today = DateTime.Today;
            string yeartdy = Convert.ToString(today.Year);
            DateTime oneweekago = today.AddDays(-14);
            int monthwk = oneweekago.Month;
            string yearwk = Convert.ToString(oneweekago.Year);
            HttpResponseMessage response = await client.GetAsync("/country/" + slug + "/status/confirmed?from=" + yearwk + "-" + monthwk + "-25T00:00:00Z&to=" + yeartdy + "-08-02T00:00:00Z");
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                List<Details> detailslist = JsonConvert.DeserializeObject<List<Details>>(data);
                ViewData["Country"] = country;
                return View(detailslist);
            }
            else
            {
                return View(new List<Details>());
            }
        }
    }
}