﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WEB2020Apr_P03_T4.DAL;
using WEB2020Apr_P03_T4.Models;

namespace Group4WEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private Package2DAL package2DB = new Package2DAL();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }
        public IActionResult ContactUs()
        {
            return View();
        }
        public IActionResult News()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }
        //qh - done by me for team component
        //qh - i used a string list because i needed to return two different strings from method
        public ActionResult UserLogin(IFormCollection formData)
        {
            string loginID = formData["txtLoginID"].ToString();
            string password = formData["txtPassword"].ToString();
            string staffRole = "Staff";
            string customerRole = "Customer";
            //qh - check if account exists
            List<string> loginstatus = package2DB.Login(loginID, password);
            //qh - validation for login
            //qh - makes sure that login status is not null
            if (loginstatus.Any())
            {
                if (loginstatus[0] == "Administrator" && loginstatus[0] != null)
                {
                    //Store Login ID in session with the key "LoginID" 
                    HttpContext.Session.SetString("LoginID", loginID);
                    //Store user role "Staff" as a string in session with the key "Role"
                    HttpContext.Session.SetString("Role", staffRole);

                    //Redirect user to the "StaffMain" view through an action 
                    return RedirectToAction("Index", "StaffMain");
                }
                else if (loginstatus[0] == "Customer" && loginstatus[0] != null)
                {
                    HttpContext.Session.SetString("LoginID", loginID);
                    HttpContext.Session.SetString("Role", customerRole);
                    HttpContext.Session.SetString("CustomerID", loginstatus[1]);
                    return RedirectToAction("Index", "Home");
                }
                else
                //qh - only admins and customer can log in. Staff and Flight attendants cannot log in
                {
                    TempData["Message"] = "Invalid Login Credentials";
                    return RedirectToAction("Login");
                }
            }
            else
            {
                TempData["Message"] = "Invalid Login Credentials";
                return RedirectToAction("Login");
            }

        }

        //qh - logs out by removing session strings
        public ActionResult Logout()
        {
            HttpContext.Session.Remove("LoginID");
            HttpContext.Session.Remove("Role");
            return RedirectToAction("Index");
        }

        public ActionResult StaffMain()
        {
            if (HttpContext.Session.GetString("Role") == null ||
                (HttpContext.Session.GetString("Role") != "Staff"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ActionResult Package2Main()
        {
            if (HttpContext.Session.GetString("Role") != "Staff")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }




    }
}