﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB2020Apr_P03_T4.Models;

namespace Group4WEB.DAL
{
    public class Package4DAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        public Package4DAL()
        {
            //Read ConnectionString from appsettings.json file     
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("AirFlightsConnectionString");
            //Instantiate a SqlConnection object with the Connection String read.        
            conn = new SqlConnection(strConn);
        }
        public List<FlightRoute> GetFlightRoutes()
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM FlightRoute ORDER BY RouteID";
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<FlightRoute> routelist = new List<FlightRoute>();

            while (reader.Read())
            {
                routelist.Add(
                    new FlightRoute
                    {
                        RouteID = reader.GetInt32(0),    //0: 1st column      
                        DepartureCity = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        DepartureCountry = reader.GetString(2), //2: 3rd column           
                        ArrivalCity = reader.GetString(3),     //3: 4th column                     
                        ArrivalCountry = reader.GetString(4),   //5: 6th column                   
                        FlightDuration = reader.GetInt32(5),  //6: 7th column                  

                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return routelist;
        }

        public List<Aircraft> GetAircraft()
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM Aircraft ORDER BY AircraftID";
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<Aircraft> aircraftlist = new List<Aircraft>();

            while (reader.Read())
            {
                aircraftlist.Add(
                    new Aircraft
                    {
                        AircraftID = reader.GetInt32(0),    //0: 1st column      
                        MakeModel = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        NumEconomySeat = reader.GetInt32(2), //2: 3rd column           
                        NumBusinessSeat = reader.GetInt32(3),     //3: 4th column                     
                        DateLastMaintenance = reader.GetDateTime(4),   //5: 6th column                   
                        Status = reader.GetString(5),  //6: 7th column                  
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return aircraftlist;
        }

        public List<FlightSchedule> GetFlightSchedules()
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM FlightSchedule ORDER BY ScheduleID";
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<FlightSchedule> scheduleList = new List<FlightSchedule>();

            while (reader.Read())
            {
                scheduleList.Add(
                    new FlightSchedule
                    {
                        ScheduleID = reader.GetInt32(0),    //0: 1st column      
                        FlightNumber = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        RouteID = reader.GetInt32(2), //2: 3rd column           
                        AircraftID = reader.GetInt32(3),     //3: 4th column                     
                        DepartureDateTime = reader.GetDateTime(4),   //5: 6th column                   
                        ArrivalDateTime = reader.GetDateTime(5),  //6: 7th column  
                        EconomyClassPrice = Convert.ToDouble(reader.GetDecimal(6)),  //6: 7th column   
                        BusinessClassPrice = Convert.ToDouble(reader.GetDecimal(7)),  //6: 7th column   
                        Status = reader.GetString(8),  //6: 7th column   
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return scheduleList;
        }
        public Aircraft GetAircraftDetails(int aircraftID) //Get Aircraft Details
        {
            Aircraft aircraft = new Aircraft();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Aircraft WHERE AircraftID = @selectedAircraftID"; //Query
            cmd.Parameters.AddWithValue("@selectedAircraftID", aircraftID);

            conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    aircraft.AircraftID = aircraftID;
                    aircraft.MakeModel = !reader.IsDBNull(1) ?
                    reader.GetString(1) : null;
                    aircraft.NumEconomySeat = !reader.IsDBNull(2) ?
                    reader.GetInt32(2) :(int)0;
                    aircraft.NumBusinessSeat = !reader.IsDBNull(3) ?
                    reader.GetInt32(3) : (int)0;
                    aircraft.DateLastMaintenance = !reader.IsDBNull(4) ?
                    reader.GetDateTime(4) : (DateTime?)null;
                    aircraft.Status = !reader.IsDBNull(5) ?
                    reader.GetString(5) : null;

                }
            }
            reader.Close();
            conn.Close();

            return aircraft;
        }
        public int AddAircraft(Aircraft aircraft)//Add Aircraft
        {
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"INSERT INTO Aircraft (MakeModel, NumEconomySeat, NumBusinessSeat, DateLastMaintenance, Status) 
                                OUTPUT INSERTED.AircraftID VALUES(@makeModel, @numEcon, @numBusiness, @lastMaintained, @status)";

            cmd.Parameters.AddWithValue("@makeModel", aircraft.MakeModel);
            cmd.Parameters.AddWithValue("@numEcon", aircraft.NumEconomySeat);
            cmd.Parameters.AddWithValue("@numBusiness", aircraft.NumBusinessSeat);
            cmd.Parameters.AddWithValue("@lastMaintained", aircraft.DateLastMaintenance);
            cmd.Parameters.AddWithValue("@status", aircraft.Status);

            conn.Open();

            aircraft.AircraftID = (int)cmd.ExecuteScalar();

            conn.Close();

            return aircraft.AircraftID;

        }
        public int AssignAircraft(int aircraftID, int scheduleID)
        {
            //Create a SqlCommand object from connection object     
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement    
            cmd.CommandText = @"UPDATE FlightSchedule SET AircraftID=@aircraftID WHERE ScheduleID = @selectedScheduleID";
            //Define the parameters used in SQL statement, value for each parameter   
            //is retrieved from respective class's property.    
            cmd.Parameters.AddWithValue("@aircraftID",aircraftID);
            cmd.Parameters.AddWithValue("@selectedScheduleID", scheduleID);
            //Open a database connection     
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE  
            int count = cmd.ExecuteNonQuery();
            //Close the database connection    
            conn.Close();

            return count;
        }
        public FlightSchedule FindSpecificFlightSchedule(int scheduleid)
        {
            FlightSchedule current = new FlightSchedule();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE ScheduleID = @selectScheduleID";
            cmd.Parameters.AddWithValue("@selectScheduleID", scheduleid);


            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    current.ScheduleID = scheduleid;
                    current.FlightNumber = !reader.IsDBNull(1) ? reader.GetString(1) : "NO VALUE";
                    // (char) 0 - ASCII Code 0 - null value       
                    current.RouteID = !reader.IsDBNull(2) ? reader.GetInt32(2) : 0;
                    current.AircraftID = !reader.IsDBNull(3) ? reader.GetInt32(3) : 1020202;
                    current.DepartureDateTime = !reader.IsDBNull(4) ? reader.GetDateTime(4) : (DateTime?)null;
                    current.ArrivalDateTime = !reader.IsDBNull(5) ? reader.GetDateTime(5) : (DateTime?)null;
                    current.EconomyClassPrice = !reader.IsDBNull(6) ? Convert.ToDouble(reader.GetDecimal(6)) : 0;
                    current.BusinessClassPrice = !reader.IsDBNull(7) ? Convert.ToDouble(reader.GetDecimal(7)) : 0;
                    current.Status = !reader.IsDBNull(8) ? reader.GetString(8) : "NO VALUE";
                }
            }



            //Close database connection  
            conn.Close();

            //Return number of row of staff record updated or deleted  
            return current;
        }
        public int UpdateStatus(Aircraft aircraft)
        {
            //Create a SqlCommand object from connection object     
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement    
            cmd.CommandText = @"UPDATE Aircraft SET Status=@status, DateLastMaintenance=@maintainence WHERE AircraftID = @selectedAircraftID";
            //Define the parameters used in SQL statement, value for each parameter   
            //is retrieved from respective class's property.    
            cmd.Parameters.AddWithValue("@status", aircraft.Status);
            cmd.Parameters.AddWithValue("@selectedAircraftID", aircraft.AircraftID);
            cmd.Parameters.AddWithValue("@maintainence", DateTime.Now);
            //Open a database connection     
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE  
            int count = cmd.ExecuteNonQuery();
            //Close the database connection    
            conn.Close();

            return count;
        }
        public List<FlightSchedule> GetAircraftFlights(int aircraftID)
        {

            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE AircraftID = @selectedAircraftID";
            cmd.Parameters.AddWithValue("selectedAircraftID", aircraftID);
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<FlightSchedule> scheduleList = new List<FlightSchedule>();

            while (reader.Read())
            {
                scheduleList.Add(
                    new FlightSchedule
                    {
                        ScheduleID = reader.GetInt32(0),    //0: 1st column      
                        FlightNumber = reader.GetString(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        RouteID = reader.GetInt32(2), //2: 3rd column           
                        AircraftID = reader.GetInt32(3),     //3: 4th column                     
                        DepartureDateTime = reader.GetDateTime(4),   //5: 6th column                   
                        ArrivalDateTime = reader.GetDateTime(5),  //6: 7th column  
                        EconomyClassPrice = Convert.ToDouble(reader.GetDecimal(6)),  //6: 7th column   
                        BusinessClassPrice = Convert.ToDouble(reader.GetDecimal(7)),  //6: 7th column   
                        Status = reader.GetString(8),  //6: 7th column   
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return scheduleList;
        }

    }
}
