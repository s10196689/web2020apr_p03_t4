﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using WEB2020Apr_P03_T4.Models;

namespace WEB2020Apr_P03_T4.Dal
{
    public class Package3DAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public Package3DAL()
        {
            //ReadConnectionStringfromappsettings.jsonfile
            var builder= new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");Configuration=builder.Build();
            string strConn=Configuration.GetConnectionString("AirFlightsConnectionString");
            //InstantiateaSqlConnectionobjectwiththe
            //ConnectionStringread.
            conn = new SqlConnection(strConn);
        }

        public List<Staff> GetStaff()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Staff ORDER BY StaffID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<Staff> stafflist = new List<Staff>();

            while (reader.Read())
            {
                stafflist.Add(
                    new Staff
                    {
                        StaffID = reader.GetInt32(0),
                        StaffName = reader.GetString(1),
                        Gender = reader.GetString(2),
                        DateEmployed = reader.GetDateTime(3),
                        Vocation = reader.GetString(4),
                        EmailAddr = reader.GetString(5),
                        Password = reader.GetString(6),
                        Status = reader.GetString(7)
                    }
                    
                    );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return stafflist;
        }

        public List<FlightCrew> GetFlight()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM FLIGHTCREW ORDER BY ScheduleID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<FlightCrew> flightlist = new List<FlightCrew>();

            while (reader.Read())
            {
                flightlist.Add(
                    new FlightCrew
                    {
                        ScheduleID = reader.GetInt32(0),    //0: 1st column
                        StaffID = reader.GetInt32(1),      //1: 2nd column
                        Role = reader.GetString(2), //2: 3rd column                       
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            return flightlist;
        }
        public int AddStaff(Staff staff)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Staff (StaffName, Gender, DateEmployed, Vocation, EmailAddr, Password, Status) 
                                OUTPUT INSERTED.staffID VALUES(@staffName, @gender, @dateEmployed, @vocation, @email, @password, @status)";

            cmd.Parameters.AddWithValue("@staffName", staff.StaffName);
            cmd.Parameters.AddWithValue("@gender", staff.Gender);
            cmd.Parameters.AddWithValue("@dateEmployed", staff.DateEmployed);
            cmd.Parameters.AddWithValue("@vocation", staff.Vocation);
            cmd.Parameters.AddWithValue("@email", staff.EmailAddr);
            cmd.Parameters.AddWithValue("@password", staff.Password);
            cmd.Parameters.AddWithValue("@status", staff.Status);

            conn.Open();

            staff.StaffID = (int)cmd.ExecuteScalar();

            conn.Close();

            return staff.StaffID;
        }
    }
}
