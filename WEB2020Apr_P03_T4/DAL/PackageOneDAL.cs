﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WEB2020Apr_P03_T4.Models;


namespace WEB2020Apr_P03_T4.Models
{
    public class PackageOneDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public PackageOneDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "AirFlightsConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public int Add(Customer customer)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Customer (CustomerName, Nationality, BirthDate, TelNo, EmailAddr, Password)
                                OUTPUT INSERTED.CustomerID
                                VALUES(@name, @nationality, @birthdate, @telno, @email, @password)";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", customer.CustomerName);
            cmd.Parameters.AddWithValue("@nationality", customer.Nationality);
            cmd.Parameters.AddWithValue("@birthdate", customer.DateTime);
            cmd.Parameters.AddWithValue("@telno", customer.TelNo);
            cmd.Parameters.AddWithValue("@email", customer.EmailAddr);
            cmd.Parameters.AddWithValue("@password", customer.Password);

            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            customer.CustomerID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return customer.CustomerID;
        }

        public bool IsEmailExist(string email)
        {
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a staff record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Customer
            WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and excute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetString(5) == email)
                        //The email address is used by another staff
                        emailFound = false;
                }
            }
            else
            { //No record
                emailFound = true; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            Console.WriteLine("emailFound");
            return emailFound;
        }

        public Customer FindCustById(int id)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Customer WHERE CustomerID = @customerid";
            cmd.Parameters.AddWithValue("@customerid", id);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            Customer c1 = new Customer();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    c1.CustomerID = id;
                    c1.CustomerName = !reader.IsDBNull(1) ? reader.GetString(1) : "N/A";
                    // (char) 0 - ASCII Code 0 - null value
                    c1.Nationality = !reader.IsDBNull(2) ? reader.GetString(2) : "N/A";
                    c1.DateTime = !reader.IsDBNull(3) ? reader.GetDateTime(3) : (DateTime?)null;
                    c1.EmailAddr = !reader.IsDBNull(4) ? reader.GetString(4) : "N/A";
                    c1.Password = !reader.IsDBNull(5) ? reader.GetString(5) : "N/A";
                }
            }

            conn.Close();
            return c1;
        }

        // Return number of row updated
        public int Updatepw(Customer customer)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE Customer SET Password = @password
                                WHERE CustomerID = @selectedCustomerID";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@password", customer.Password);
            cmd.Parameters.AddWithValue("@selectedCustomerID", customer.CustomerID);
            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();
            return count;
        }

        public List<Booking> GetBookingsForCustomer(int id)
        {
            //Create a SqlCommand object from connection object   
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement         
            cmd.CommandText = @"SELECT * FROM Booking WHERE CustomerID = @CustomerID";
            cmd.Parameters.AddWithValue("@CustomerID", id);
            //Open a database connection            
            conn.Open();
            //Execute the SELECT SQL through a DataReader     
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list           
            List<Booking> bookingList = new List<Booking>();

            while (reader.Read())
            {
                bookingList.Add(
                    new Booking
                    {
                        BookingID = reader.GetInt32(0),    //0: 1st column      
                        CustomerID = reader.GetInt32(1),      //1: 2nd column          
                        //Get the first character of a string                  
                        ScheduleID = reader.GetInt32(2), //2: 3rd column           
                        PassengerName = reader.GetString(3),     //3: 4th column          
                        PassportNumber = reader.GetString(4),
                        Nationality = reader.GetString(5),   //5: 6th column                   
                        SeatClass = reader.GetString(6),  //6: 7th column  
                        AmtPayable = Convert.ToDecimal(reader.GetDecimal(7)),  //6: 7th column   
                        Remarks = !reader.IsDBNull(8) ? reader.GetString(8) : "NO REMARK",  //6: 7th column   
                        DateTimeCreated = reader.GetDateTime(9),  //6: 7th column   
                    }
                );
            }

            //Close DataReader        
            reader.Close();
            //Close the database connection   
            conn.Close();

            return bookingList;
        }

        public Booking GetDetails(int bookingId)
        {
            Booking booking = new Booking();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM Booking
                            WHERE BookingID = @selectedBookingID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@selectedBookingID", bookingId);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader
                    booking.BookingID = bookingId; //row 1
                    booking.CustomerID = reader.GetInt32(1); //row 2
                    booking.ScheduleID = reader.GetInt32(2); //row 3
                    booking.PassengerName = !reader.IsDBNull(3) ?
                    reader.GetString(3) : null; //row 4
                    booking.PassportNumber = !reader.IsDBNull(4) ?
                    reader.GetString(4) : null; //row 5
                    booking.Nationality = !reader.IsDBNull(5) ?
                    reader.GetString(5) : null; //row 6
                    booking.SeatClass = !reader.IsDBNull(6) ?
                    reader.GetString(6) : null; //row 7
                    booking.AmtPayable = reader.GetDecimal(7); //row 8
                    booking.Remarks = !reader.IsDBNull(8) ?
                    reader.GetString(8) : null; //row 9
                    booking.DateTimeCreated = !reader.IsDBNull(9) ?
                    reader.GetDateTime(9) : (DateTime?)null;
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return booking;
        }

        public int AddBooking(Booking booking)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Booking (CustomerID, ScheduleID, PassengerName, PassportNumber, Nationality, SeatClass, AmtPayable, Remarks, DateTimeCreated)
                                OUTPUT INSERTED.BookingID
                                VALUES(@customerID, @scheduleID, @passengerName, @passportNo, @nationality, @seatClass, @amtPayable, @remarks, @dateTimeCreated)";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@customerID", booking.CustomerID);
            cmd.Parameters.AddWithValue("@scheduleID", booking.ScheduleID);
            cmd.Parameters.AddWithValue("@passengerName", booking.PassengerName);
            cmd.Parameters.AddWithValue("@passportNo", booking.PassportNumber);
            cmd.Parameters.AddWithValue("@nationality", booking.Nationality);
            cmd.Parameters.AddWithValue("@seatClass", booking.SeatClass);
            cmd.Parameters.AddWithValue("@remarks", booking.Remarks);
            cmd.Parameters.AddWithValue("@amtPayable", booking.AmtPayable);
            cmd.Parameters.AddWithValue("@dateTimeCreated", DateTime.Now);


            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            booking.BookingID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return booking.BookingID;
        }
    }
}