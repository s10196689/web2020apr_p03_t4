﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2020Apr_P03_T4.Models
{
    public class FlightScheduleViewModel
    {
        [Display (Name = "Aircraft ID")]
        public int AircraftID { get; set; }
        [Display(Name ="Schedule ID")]
        public int ScheduleID { get; set; }

        [Display(Name = "Flight Number")]

        public string FlightNumber { get; set; }

        [Display (Name = "Departure Date and Time")]

        public DateTime? DepartureDateTime { get; set; }

        [Display (Name = "Arrival Date and Time")]

        public DateTime? ArrivalDateTime { get; set; }

        [Display (Name = "City of Departure")]

        public string DepartureCity { get; set; }

        [Display (Name = "City of Arrival")]

        public string ArrivalCity { get; set; }


    }
}
