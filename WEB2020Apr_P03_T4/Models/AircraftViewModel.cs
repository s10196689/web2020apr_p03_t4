﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2020Apr_P03_T4.Models
{
    public class AircraftViewModel
    {
        [Display (Name = "AircraftID")]
        public int AircraftID { get; set; }
        [Display (Name = "Make Model")]
        public string MakeModel { get; set; }
        [Display(Name = "Number of economy seats")]
        public int NumEconomySeat { get; set; }
        [Display(Name = "Number of business seats")]
        public int NumBusinessSeat { get; set; }
        [Display(Name = "Date of last maintenance")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateLastMaintenance { get; set; }

        [Display (Name = "City of Departure")]
        public string DepartureCity { get; set; }
        [Display(Name = "City of Arrival")]
        public string ArrivalCity { get; set; }
        [Display(Name = "Departure Date and time")]
        public DateTime? DepartureDateTime { get; set; }
        [Display(Name = "Arrival Date and Time")]
        public DateTime? ArrivalDateTime { get; set; }
        [Display (Name = "Status")]
        public string Status { get; set; }
        [Display (Name = "Date Of last Maintainence")]
        public DateTime? DateOfLastMaintenance { get; set; }

        [Display (Name = "Aircraft Flight Schedules")]

        public List<FlightScheduleViewModel> AircraftHistory { get; set; }
        [Display(Name = "All Flight Schedules")]

        public List<FlightScheduleViewModel> AllFlights { get; set; }


    }
}
