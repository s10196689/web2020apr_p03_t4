﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace WEB2020Apr_P03_T4.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }

        [Required]
        public string CustomerName { get; set; }

        [Required]
        public string Nationality { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime? DateTime { get; set; }

        [Required]
        public string TelNo { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailAddr { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
