﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace WEB2020Apr_P03_T4.Models
{
    public class Aircraft
    {
        [Display (Name = "AircraftID")]
        public int AircraftID { get; set; }
        [Display(Name = "Make Model")]
        public string MakeModel { get; set; }
        [Display(Name = "Number of economy seats")]
        public int NumEconomySeat { get; set; }
        [Display(Name = "Number of business seats")]
        public int NumBusinessSeat { get; set; }
        [Display(Name = "Date of last maintenance")]
        public DateTime? DateLastMaintenance { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}

