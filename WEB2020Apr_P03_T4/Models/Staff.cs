﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2020Apr_P03_T4.Models
{
    public class Staff
    {
        [Required]
        public int StaffID { get; set; }

        [Required]
        public string StaffName { get; set; }

        [Required]
        public string? Gender { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateEmployed { get; set; }

        [Required]
        public string? Vocation { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailAddr { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public string Status { get; set; }

    }
}
