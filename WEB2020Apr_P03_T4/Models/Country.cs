﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2020Apr_P03_T4.Models
{
    public class Country
    {
        public string country { get; set; }

        public string slug { get; set; }

        public string ISO2 { get; set; }
    }

    public class Details
    {
        public string country { get; set; }

        public string countrycode { get; set; }

        public string province { get; set; }

        public string city { get; set; }

        public string citycode { get; set; }

        public string lat { get; set; }

        public string lon { get; set; }

        public int cases { get; set; }

        public string status { get; set; }

        public string date { get; set; }
    }
}
