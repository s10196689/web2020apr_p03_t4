﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace WEB2020Apr_P03_T4.Models
{
    public class StaffView
    {
        [Required]
        public string StaffName { get; set; }

        [Required]
        public string Vocation { get; set; }

        [Required]
        public int ScheduleID { get; set; }

        [Required]
        public string Status { get; set; }
    }
}
