﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2020Apr_P03_T4.Models
{
    public class FlightCrew
    {
        [Required]
        public int ScheduleID { get; set; }

        [Required]
        public int StaffID { get; set; }

        [Required]
        public string Role { get; set; }

    }
}