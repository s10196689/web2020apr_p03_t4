﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace WEB2020Apr_P03_T4.Models
{
    public class FlightRoute
    {
        [Required]
        public int RouteID { get; set; }
        [Required]
        public string DepartureCity { get; set; }
        [Required]
        public string DepartureCountry { get; set; }
        [Required]
        public string ArrivalCity { get; set; }
        [Required]
        public string ArrivalCountry { get; set; }
        [Required]
        public int FlightDuration { get; set; }


    }
}
